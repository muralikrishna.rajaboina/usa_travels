exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('hotelbedsdestinations', function(table) {
      table.increments('id').primary();
      table.string('destinationCode');
      table.string('destinationArea');
      table.string('destinationName');
      table.boolean('isActive');
      table
        .timestamp('created_at')
        .notNullable()
        .defaultTo(knex.fn.now());
      table
        .timestamp('updated_at')
        .notNullable()
        .defaultTo(knex.fn.now());
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([knex.schema.dropTable('hotelbedsdestinations')]);
};

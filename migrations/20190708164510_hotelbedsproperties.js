exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('hotelbedsProperties', function(table) {
      table.increments('id').primary();
      table.string('propertyId');
      table.string('name');
      table.string('rating');
      table.string('destinationCode');
      table.string('latitude');
      table.string('longitude');
      table.boolean('isActive');
      table
        .timestamp('created_at')
        .notNullable()
        .defaultTo(knex.fn.now());
      table
        .timestamp('updated_at')
        .notNullable()
        .defaultTo(knex.fn.now());
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([knex.schema.dropTable('hotelbedsProperties')]);
};

// Update with your config settings.

module.exports = {
  development: {
    client: 'pg',
    connection: {
      database: 'usatravels',
      host: 'localhost',
      user: 'postgres',
      password: 'Design_20'
    },
    pool: {
      min: 0,
      max: 10
    }
  },

  staging: {
    client: 'pg',
    connection: {
      database: 'usatravels',
      user: 'postgres',
      password: 'Design_20'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'Production'
    }
  },

  production: {
    client: 'pg',
    connection: {
      database: 'usatravels',
      user: 'postgres',
      password: 'Design_20'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'Production'
    }
  }
};

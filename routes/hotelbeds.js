const express = require('express');
const router = express.Router();
const axios = require('axios');
var sha256 = require('sha256');
const knex = require('knex');
const config = require('../knexfile').development;
const xlsxtojson = require('xlsx-to-json-lc');
const xlstojson = require('xls-to-json-lc');
const multer = require('multer');
const storage = multer.diskStorage({
  //multers disk storage settings
  destination: function(req, file, cb) {
    console.log('murali');
    cb(null, './uploads/');
  },
  filename: function(req, file, cb) {
    console.log('krishna');

    cb(
      null,
      file.fieldname +
        '.' +
        file.originalname.split('.')[file.originalname.split('.').length - 1]
    );
  }
});

const upload = multer({
  //multer settings

  storage: storage,
  limits: {
    fileSize: 52428800
  },
  fileFilter: function(req, file, callback) {
    //file filter
    console.log('check file extension');
    if (
      ['xls', 'xlsx'].indexOf(
        file.originalname.split('.')[file.originalname.split('.').length - 1]
      ) === -1
    ) {
      return callback(new Error('Wrong extension type'));
    }
    callback(null, true);
  }
}).single('file');

router.post('/destinationsdata', function(req, res) {
  console.log('xlsx to json conversion');
  req.setTimeout(0);
  let exceltojson;
  upload(req, res, function(err) {
    if (err) {
      res.status(400).json({ error_code: 1, err_desc: err });
      return;
    }

    if (!req.file) {
      console.log('sdggh');
      res.status(400).json({ error_code: 1, err_desc: 'No file passed' });
      return;
    }

    if (
      req.file.originalname.split('.')[
        req.file.originalname.split('.').length - 1
      ] === 'xlsx'
    ) {
      exceltojson = xlsxtojson;
    } else {
      exceltojson = xlstojson;
    }

    try {
      exceltojson(
        {
          input: req.file.path, //the same path where we uploaded our file
          output: null, //since we don't need output.json
          lowerCaseHeaders: false
        },
        function(err, result) {
          if (err) {
            return res
              .status(400)
              .json({ error_code: 1, err_desc: err, data: null });
          }
          console.log(result);
          const result1 = result.map(o => ({
            destinationCode: o['destinations'],
            destinationArea: o['area'],
            destinationName: o['name'],
            isActive: true,
            created_at: new Date().toISOString(),
            updated_at: new Date().toISOString()
          }));
          const db = knex(config);
          db.transaction(function(tr) {
            return knex(config)
              .batchInsert('hotelbedsdestinations', result1)
              .transacting(tr);
          })
            .then(() => {
              db.destroy();
              return res.status(200).json({ message: 'success' });
            })
            .catch(error => {
              db.destroy();
              return res.status(400).json({ message: error.message });
            });
        }
      );
    } catch (e) {
      db.destroy();
      res.status(400).json({ error_code: 1, err_desc: 'Corrupted excel file' });
    }
  });
});

router.post('/propertiesdata', function(req, res) {
  console.log('xlsx to json conversion');
  req.setTimeout(0);
  let exceltojson;
  upload(req, res, function(err) {
    if (err) {
      res.status(400).json({ error_code: 1, err_desc: err });
      return;
    }

    if (!req.file) {
      console.log('sdggh');
      res.status(400).json({ error_code: 1, err_desc: 'No file passed' });
      return;
    }

    if (
      req.file.originalname.split('.')[
        req.file.originalname.split('.').length - 1
      ] === 'xlsx'
    ) {
      exceltojson = xlsxtojson;
    } else {
      exceltojson = xlstojson;
    }

    try {
      exceltojson(
        {
          input: req.file.path, //the same path where we uploaded our file
          output: null, //since we don't need output.json
          lowerCaseHeaders: false
        },
        function(err, result) {
          if (err) {
            return res
              .status(400)
              .json({ error_code: 1, err_desc: err, data: null });
          }
          console.log(result);
          const result1 = result.map(o => ({
            propertyId: o['propertyId'],
            name: o['name'],
            rating: o['rating'],
            destinationCode: o['destinationCode'],
            latitude: o['latitude'],
            longitude: o['longitude'],
            isActive: true,
            created_at: new Date().toISOString(),
            updated_at: new Date().toISOString()
          }));
          const db = knex(config);
          db.transaction(function(tr) {
            return knex(config)
              .batchInsert('hotelbedsProperties', result1)
              .transacting(tr);
          })
            .then(() => {
              db.destroy();
              return res.status(200).json({ message: 'success' });
            })
            .catch(error => {
              db.destroy();
              return res.status(400).json({ message: error.message });
            });
        }
      );
    } catch (e) {
      db.destroy();
      res.status(400).json({ error_code: 1, err_desc: 'Corrupted excel file' });
    }
  });
});

function get_X_signature() {
  var apikey = '68f28e4p29dtqyvqdttjv3kb';
  var secret = 'Mj794eq8cS';
  var time_sec = Math.trunc(new Date().getTime() / 1000);
  var sig = apikey + secret + time_sec;
  var X_sig = sha256(sig);
  return X_sig;
}
const get_paxes = request => {
  console.log(request);
  paxes = [];
  for (let i = 1; i < 10; i++) {
    if (Number(request['r' + i + 'k']) > 0) {
      request['r' + i + 'ka'].map(x => paxes.push({ type: 'CH', age: x }));
      console.log(paxes);
    }
  }
  return paxes;
};

let date_diff_indays = function(request) {
  dt1 = new Date(request.checkIn);
  dt2 = new Date(request.checkOut);
  return Math.floor(
    (Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) -
      Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) /
      (1000 * 60 * 60 * 24)
  );
};
let get_date = (date, days) => {
  var d1 = new Date(date);
  d1.setDate(d1.getDate() + days);
  return d1.getFullYear() + '-' + d1.getMonth() + '-' + d1.getDate();
};
// router.get('/availability', async function(req, res, next) {
//   try {
//     db = knex(config);

//     const destinationCode = await db
//       .select('destinationCode')
//       .from('hotelbedsdestinations')
//       .where('destinationName', 'ilike', '%' + request.destinationName + '%');

//     const propertyCodes = await db
//       .select('propertyId')
//       .from('hotelbedsProperties')
//       .where('destinationCode', destinationCode[0].destinationCode);

//     const allProperties = propertyCodes.map(x => parseInt(x.propertyId));
//     console.log(allProperties);

//     console.log(destinationCode[0]);

//     const get_paxes = request => {
//       paxes = [];
//       for (let i = 1; i < 10; i++) {
//         if (request['r' + i + 'k']) {
//           request['r' + i + 'ka'].map(x => paxes.push({ type: 'CH', age: x }));
//         }
//       }
//     };

//     const roomDet = request => {
//       let roomDetails = [];
//       for (let i = 1; i < 10; i++) {
//         if (request['r' + i + 'a'] || request['r' + i + 'k']) {
//           roomDetails.push({
//             seqnum: i,
//             adultNum: request['r' + i + 'a'],
//             childNum: request['r' + i + 'k']
//           });
//         }
//       }
//       return roomDetails;
//     };

//     const get_date = (date, days) => {
//       var d1 = new Date(date);
//       d1.setDate(d1.getDate() + days);
//       return d1.getFullYear() + '-' + d1.getMonth() + '-' + d1.getDate();
//     };

//     const response = await axios({
//       url: `https://api.test.hotelbeds.com/hotel-api/1.0/hotels`,
//       method: 'POST',
//       headers: {
//         'Api-key': '68f28e4p29dtqyvqdttjv3kb',
//         'X-Signature': get_X_signature(),
//         Accept: 'application/json',
//         'Accept-Encoding': 'gzip',
//         'Content-Type': 'application/json'
//       },
//       data: {
//         stay: {
//           checkIn: request.checkIn,
//           checkOut: request.checkOut
//         },
//         occupancies: [
//           {
//             rooms: request.rc,
//             adults:
//               request.r1a +
//               request.r2a +
//               request.r3a +
//               request.r4a +
//               request.r5a +
//               request.r6a +
//               request.r7a +
//               request.r8a +
//               request.r9a,
//             children:
//               request.r1k +
//               request.r2k +
//               request.r3k +
//               request.r4k +
//               request.r5k +
//               request.r6k +
//               request.r7k +
//               request.r8k +
//               request.r9k,
//             paxes: get_paxes(request)
//           }
//         ],
//         hotels: {
//           hotel: allProperties
//         }
//       }
//     });
//     const result = await response.data;
//     const output = result['hotels']['hotels'].map(x => ({
//       hotelId: x['code'],
//       provider: 'hotel beds',
//       hotelName: x['name'],
//       description: '',
//       starRating: Number(x['categoryName'][0]),
//       address: x['destinationName'],
//       location: '',
//       hotelImageUrl: '',
//       price: Number(x['minRate']),
//       room: [].concat(
//         ...x['rooms'].map(y =>
//           y['rates'].map(z => ({
//             roomId: y['name'],
//             priceInfo: {
//               occupancy: [
//                 {
//                   priceBreak: {
//                     priceR: [].concat(
//                       ...[...Array(z['allotment']).keys()].map(a => ({
//                         value: a,
//                         price: z['net'] / (z['allotment'] * z['rooms']),
//                         pricePublish: 0,
//                         from: get_date(request.checkIn, a),
//                         to: ' '
//                       }))
//                     )
//                   },
//                   roomDet: roomDet(request)
//                 }
//               ]
//             },
//             discount: {
//               from: '',
//               to: '',
//               disType: '',
//               value: '',
//               name: '',
//               actualPrice: 0,
//               price: 0
//             },
//             essentialInfo: [
//               {
//                 text: '',
//                 fromDate: '',
//                 toDate: ''
//               }
//             ],
//             baseAmount: 0,
//             roomConfirmation: '',
//             chargeConditions: [
//               {
//                 charge: '',
//                 chargeAmount: 0,
//                 currency: 'USD',
//                 fromDay: 0,
//                 toDay: 0,
//                 firstDay: "doesn't having cancellation deadline",
//                 description: "doesn't having cancellation policy"
//               }
//             ],
//             roomType: y['name'],
//             roomTypeName: y['name'],
//             price: z['net'] / (z['allotment'] * z['rooms']),
//             totalPrice: Number(z['net']),
//             currency: 'USD',
//             maxGuests: 2,
//             provider: 'hotelbeds',
//             mealPlan: z['boardName'],
//             supplements: [],
//             boardBases: [],
//             rateKey: z['rateKey'],
//             marketId: '',
//             contractId: '',
//             tax: 0,
//             id: '0'
//           }))
//         )
//       )
//     }));
//     return res.send(output);
//   } catch (err) {
//     return res.status(500).json(err);
//   }
// });

router.get('/hoteldetails/:id', async function(req, res, next) {
  try {
    console.log(req.params.id);
    const url1 = new URL(
      req.params.id,
      'https://api.test.hotelbeds.com/hotel-content-api/1.0/hotels/'
    );
    const response = await axios({
      url: url1.href,
      method: 'GET',
      headers: {
        'Api-key': '68f28e4p29dtqyvqdttjv3kb',
        'X-Signature': get_X_signature(),
        Accept: 'application/json',
        'Accept-Encoding': 'gzip'
      },
      params: { language: 'ENG', useSecondaryLanguage: 'false' }
    });
    const result = await response.data;
    const output = {
      hotelId: result['hotel']['code'],
      hotelName: result['hotel']['name']['content'],
      hotelFax:
        result['hotel']['phones'].filter(p => p.phoneType === 'FAXNUMBER')
          .length === 0
          ? ''
          : result['hotel']['phones'].filter(
              p => p.phoneType === 'FAXNUMBER'
            )[0]['phoneNumber'],
      hotelPhone:
        result['hotel']['phones'].filter(p => p.phoneType === 'PHONEHOTEL')
          .length === 0
          ? ''
          : result['hotel']['phones'].filter(
              p => p['phoneType'] === 'PHONEHOTEL'
            )[0]['phoneNumber'],
      address: result['hotel']['address']['content'],
      description: result['hotel']['description']['content'],
      hotelImages: result['hotel']['images'].map(
        i => 'http://photos.hotelbeds.com/giata/' + i['path']
      ),
      amenities: [],
      roomDetails: result['hotel']['rooms'].map(r => ({
        roomType: r['type']['description']['content'],
        roomTypeCode: r['type']['code'],
        maxGuests: 2,
        pricePerNight: 0,
        facilities: r['roomFacilities'] ? r['roomFacilities'] : [],
        imageUrl: '',
        hotelRoomTypeIds: result['hotel']['rooms'].map(i => i['roomCode'])
      })),
      latitude: result['hotel']['coordinates']['latitude'],
      longitude: result['hotel']['coordinates']['longitude']
    };
    return res.send(output);
    // return res.send(result)
    // return res.send(result['hotel']['facilities'])
  } catch (err) {
    return res.status(500).json(err);
  }
});

router.get('/hotels', async function(req, res, next) {
  try {
    const response = await axios({
      url:
        'https://api.test.hotelbeds.com/hotel-content-api/1.0/locations/countries?lastUpdateTime=2015-09-10&fields=All&language=ENG',
      method: 'GET',
      headers: {
        'Api-key': '68f28e4p29dtqyvqdttjv3kb',
        'X-Signature': get_X_signature(),
        Accept: 'application/json',
        'Accept-Encoding': 'gzip'
      }
    });
    return res.send(response.data);
  } catch (err) {
    return res.status(500).json(err);
  }
});

router.get('/destinations', async function(req, res, next) {
  try {
    const response = await axios({
      url:
        'https://api.test.hotelbeds.com/hotel-content-api/1.0/locations/destinations?fields=all&lastUpdateTime=2015-09-10&language=ENG&from=1&to=100&useSecondaryLanguage=false',
      method: 'GET',
      headers: {
        'Api-key': '68f28e4p29dtqyvqdttjv3kb',
        'X-Signature': get_X_signature(),
        Accept: 'application/json',
        'Accept-Encoding': 'gzip'
      }
    });
    return res.send(response.data);
  } catch (err) {
    return res.status(500).json(err);
  }
});
router.get('/test', async function(req, res, next) {
  return res.json({ message: 'success' }).catch(e => {
    console.log('error');
    return e;
  });
});

router.get('/book', async function(req, res, next) {
  try {
    const response = await axios({
      url:
        'https://api.test.hotelbeds.com/hotel-content-api/1.0/locations/countries?lastUpdateTime=2015-09-10&fields=All&language=ENG',
      method: 'GET',
      headers: {
        'Api-key': '68f28e4p29dtqyvqdttjv3kb',
        'X-Signature': get_X_signature(),
        Accept: 'application/json',
        'Accept-Encoding': 'gzip'
      }
    });
    return res.send(response.data);
  } catch (err) {
    return res.status(500).json(err);
  }
});

router.get('/availabilitysearch', async function(req, res, next) {
  try {
    console.log('murali');

    return res.send(req.query);
    // return res.send(result)
  } catch (err) {
    return res.status(500).json(err);
  }
});

// router.get('/olympusfile', async (req, res) => {
//   let db = knex(config);

//   db = knex(config);
//   Array.prototype.unique = function() {
//     var arr = [];
//     for (var i = 0; i < this.length; i++) {
//       if (!arr.includes(this[i])) {
//         arr.push(this[i]);
//       }
//     }
//     return arr;
//   };

//   const data = await db
//     .select('propertyId', 'name', 'destinationCode', 'latitude', 'longitude')
//     .from('hotelbedsProperties');
//   const destinationdata = await db
//     .select('destinationName', 'destinationCode')
//     .from('hotelbedsdestinations');
//   const val = destinationdata.map(x => {
//     destinationCode: x.destinationCode,destinationName x.destinationName;
//   });
//   //.unique();

//   // const log = data.map(x =>
//   //   val
//   //     .filter(y => y.destinationCode === x.destinationCode)
//   //     .map(k => k.destinationName[0])
//   // );
//   // console.log(val);
//   // console.log(destinationdata.filter(x=>));
//   //   jsonexport(logdata, function(err, csv) {
//   //     if (err) return console.log(err);
//   //     fs.writeFileSync('./uploads/data.csv', csv, 'binary');
//   //   });

//   //  return res.download('./uploads/data.csv', 'data.csv');
//   return res.json(val);
//   // } catch (error) {
//   //   await db.destroy();
//   //   return res.status(500).json({
//   //     message: 'some thing awefull happened'
//   //   });
//   // }
// });

module.exports = router;

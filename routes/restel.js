var express = require('express');
var router = express.Router();
var soap = require('soap');
/* GET home page. */
const soapRequest = require('easy-soap-request');
var parser = require('xml2json');
router.get('/testsoap', async (req, res) => {
  const { response } = await soapRequest(
    'http://xml.hotelresb2b.com/xml/listen_xml.jsp?codigousu=HLDV&clausu=xml493185&afiliacio=RS&secacc=125114&xml=%3C%3Fxml+version%3D%221.0%22+encoding%3D%22ISO-8859-1%22+%3F%3E+%0D%0A%3Cpeticion%3E%0D%0A%09%3Ctipo%3E110%3C%2Ftipo%3E+%0D%0A%09%3Cnombre%3EDisponibilidad+Prueba%3C%2Fnombre%3E+%0D%0A%09%3Cagencia%3EAgencia+Prueba%3C%2Fagencia%3E+%0D%0A%09%3Cparametros%3E%0D%0A%09+%09%3Chotel%3E745388%23%3C%2Fhotel%3E+%0D%0A%09%09%3Cpais%3EES%3C%2Fpais%3E%0D%0A%09%09%3Cprovincia%3EESNAV%3C%2Fprovincia%3E%0D%0A%09%09%3Cpoblacion%3E%3C%2Fpoblacion%3E%0D%0A%09%09%3Ccategoria%3E0%3C%2Fcategoria%3E%0D%0A%09%09%3Cradio%3E9%3C%2Fradio%3E%0D%0A%09%09%3Cfechaentrada%3E05%2F16%2F2019%3C%2Ffechaentrada%3E%0D%0A%09%09%3Cfechasalida%3E05%2F17%2F2019%3C%2Ffechasalida%3E%0D%0A%09%09%3Cmarca%3E%3C%2Fmarca%3E%0D%0A%09%09%3Cafiliacion%3ERS%3C%2Fafiliacion%3E%0D%0A%09%09%3Cusuario%3E13460%3C%2Fusuario%3E%0D%0A%09%09%3Cnumhab1%3E1%3C%2Fnumhab1%3E+%0D%0A%09%09%3Cpaxes1%3E2-0%3C%2Fpaxes1%3E+%0D%0A%09%09%3Cedades1%3E%3C%2Fedades1%3E%0D%0A%09%09%3Cnumhab2%3E0%3C%2Fnumhab2%3E+%0D%0A%09%09%3Cpaxes2%3E2-0%3C%2Fpaxes2%3E+%0D%0A%09%09%3Cedades2%3E%3C%2Fedades2%3E%0D%0A%09%09%3Cnumhab3%3E0%3C%2Fnumhab3%3E+%0D%0A%09%09%3Cpaxes3%3E2-0%3C%2Fpaxes3%3E+%0D%0A%09%09%3Cedades3%3E%3C%2Fedades3%3E%0D%0A%09%09%3Crestricciones%3E1%3C%2Frestricciones%3E+%0D%0A%09%09%3Cidioma%3E2%3C%2Fidioma%3E%0D%0A%09%09%3Cduplicidad%3E0%3C%2Fduplicidad%3E%0D%0A%09%09%3Cinformacion_hotel%3E0%3C%2Finformacion_hotel%3E%0D%0A%09%09%3Ctarifas_reembolsables%3E0%3C%2Ftarifas_reembolsables%3E%0D%0A%09%09%3Ccomprimido%3E2%3C%2Fcomprimido%3E%0D%0A%09%3C%2Fparametros%3E%0D%0A%3C%2Fpeticion%3E',
    `<peticion>
      <tipo>110</tipo>
      <nombre>Servicio de disponibilidad por lista de hoteles</nombre>
      <agencia>Agencia Prueba</agencia>
      <parametros>
      <hotel>745388#</hotel>
      <pais>MV</pais>
      <pais_cliente>ES</pais_cliente>
      <categoria>0</categoria>
      <fechaentrada>08/15/2018</fechaentrada>
      <fechasalida>08/16/2018</fechasalida>
      <afiliacion>RS</afiliacion>
      <usuario>XXXXXXXX</usuario>
      <numhab1>1</numhab1>
      <paxes1>2-0</paxes1>
      <edades1></edades1>
      <numhab2>0</numhab2>
      <paxes2>2-0</paxes2>
      <edades2></edades2>
      <numhab3>0</numhab3>
      <paxes3>2-0</paxes3>
      <edades3></edades3>
      <idioma>2</idioma>
      <informacion_hotel>0</informacion_hotel>
      <tarifas_reembolsables>1</tarifas_reembolsables>
      <comprimido>2</comprimido>
      </parametros>
      </peticion>`
  );
  // const { body } = response;
  console.log('successs');
  return res.json(response).catch(error => {
    db.destroy();
    return error;
  });
});

router.get('/nodesaoptest', async (req, res) => {
  try {
    var url = 'http://xml.hotelresb2b.com/xml/listen_xml.jsp';
    var args = { name: 'value' };

    const data = await soap.createClient(
      'http://xml.hotelresb2b.com/xml/listen_xml.jsp?codigousu=HLDV&clausu=xml493185&afiliacio=RS&secacc=125114&xml=',
      function(err, client) {
        client.MyFunction(
          `<peticion>
      <tipo>110</tipo>
      <nombre>Servicio de disponibilidad por lista de hoteles</nombre>
      <agencia>Agencia Prueba</agencia>
      <parametros>
      <hotel>745388#</hotel>
      <pais>MV</pais>
      <pais_cliente>ES</pais_cliente>
      <categoria>0</categoria>
      <fechaentrada>08/15/2018</fechaentrada>
      <fechasalida>08/16/2018</fechasalida>
      <afiliacion>RS</afiliacion>
      <usuario>XXXXXXXX</usuario>
      <numhab1>1</numhab1>
      <paxes1>2-0</paxes1>
      <edades1></edades1>
      <numhab2>0</numhab2>
      <paxes2>2-0</paxes2>
      <edades2></edades2>
      <numhab3>0</numhab3>
      <paxes3>2-0</paxes3>
      <edades3></edades3>
      <idioma>2</idioma>
      <informacion_hotel>0</informacion_hotel>
      <tarifas_reembolsables>1</tarifas_reembolsables>
      <comprimido>2</comprimido>
      </parametros>
      </peticion>`,
          function(err, result) {
            console.log(result);
          }
        );
      }
    );
    return res.json(data);
  } catch (error) {
    console.log(error);

    return res.json({ message: error });
  }
});

module.exports = router;

const express = require('express');
const router = express.Router();
const axios = require('axios');
const parser = require('xml2json');
const knex = require('knex');
const config = require('../knexfile').development;
const xlsxtojson = require('xlsx-to-json-lc');
const xlstojson = require('xls-to-json-lc');
const multer = require('multer');
const FormData = require('form-data');
const qs = require('qs');
const moment = require('moment');

const searchDetails = request => {
  let r1Details = [];
  let r2Details = [];
  let r3Details = [];
  let r4Details = [];
  let r5Details = [];
  let r6Details = [];
  let r7Details = [];
  let r8Details = [];
  let r9Details = [];
  if (request.r1a) {
    for (let j = 0; j < request.r1a; ++j) {
      r1Details.push(18);
    }
    if (request.r1ka) {
      r1Details = r1Details
        .concat(request.r1ka.slice(0, request.r1k))
        .map(x => parseFloat(x));
    }
  }

  if (request.r2a) {
    for (let j = 0; j < request.r2a; ++j) {
      r2Details.push(18);
    }
    if (request.r2ka) {
      r2Details = r2Details
        .concat(request.r2ka.slice(0, request.r2k))
        .map(x => parseFloat(x));
    }
  }
  if (request.r3a) {
    for (let j = 0; j < request.r3a; ++j) {
      r3Details.push(18);
    }
    if (request.r3ka) {
      r3Details = r3Details
        .concat(request.r3ka.slice(0, request.r3k))
        .map(x => parseFloat(x));
    }
  }
  if (request.r4a) {
    for (let j = 0; j < request.r4a; ++j) {
      r4Details.push(18);
    }
    if (request.r4ka) {
      r4Details = r4Details
        .concat(request.r4ka.slice(0, request.r4k))
        .map(x => parseFloat(x));
    }
  }
  if (request.r5a) {
    for (let j = 0; j < request.r5a; ++j) {
      r5Details.push(18);
    }
    if (request.r5ka) {
      r5Details = r5Details
        .concat(request.slice(0, request.r5k))
        .map(x => parseFloat(x));
    }
  }

  if (request.r6a) {
    for (let j = 0; j < request.r6a; ++j) {
      r6Details.push(18);
    }
    if (request.r6ka) {
      r6Details = r6Details
        .concat(request.r6ka.slice(0, request.r6k))
        .map(x => parseFloat(x));
    }
  }
  if (request.r7a) {
    for (let j = 0; j < request.r7a; ++j) {
      r7Details.push(18);
    }
    if (request.r7ka) {
      r7Details = r7Details
        .concat(request.r7ka.slice(0, request.r7k))
        .map(x => parseFloat(x));
    }
  }
  if (request.r8a) {
    for (let j = 0; j < request.r8a; ++j) {
      r8Details.push(18);
    }
    if (request.r8ka) {
      r8Details = r8Details
        .concat(request.r8ka.slice(0, request.r8k))
        .map(x => parseFloat(x));
    }
  }
  if (request.r9a) {
    for (let j = 0; j < request.r9a; ++j) {
      r9Details.push(18);
    }
    if (request.r9ka) {
      r9Details = r9Details
        .concat(request.r9ka.slice(0, request.r9k))
        .map(x => parseFloat(x));
    }
  }
  let roomsDetails = JSON.stringify(
    [
      r1Details,
      r2Details,
      r3Details,
      r4Details,
      r5Details,
      r6Details,
      r7Details,
      r8Details,
      r9Details
    ].filter(x => x.length !== 0)
  );
  return roomsDetails;
};
const dateformat = date => {
  let day = '';
  if (new Date(date).getDate() < 10) {
    day = '0' + new Date(date).getDate();
  } else {
    day = new Date(date).getDate();
  }

  let month = '';
  if (new Date(date).getMonth() + 1 < 10) {
    month = '0' + (new Date(date).getMonth() + 1);
  } else {
    month = new Date(date).getMonth() + 1;
  }
  const newdate = new Date(date).getFullYear() + '-' + month + '-' + day;

  return newdate;
};
//To get all properties ofa hotel id,name,address,lacality,region,postalcode,country,latitude,longitude.
const storage = multer.diskStorage({
  //multers disk storage settings
  destination: function(req, file, cb) {
    console.log('murali');
    cb(null, './uploads/');
  },
  filename: function(req, file, cb) {
    console.log('krishna');

    cb(
      null,
      file.fieldname +
        '.' +
        file.originalname.split('.')[file.originalname.split('.').length - 1]
    );
  }
});

const upload = multer({
  //multer settings

  storage: storage,
  limits: {
    fileSize: 52428800
  },
  fileFilter: function(req, file, callback) {
    //file filter
    console.log('check file extension');
    if (
      ['xls', 'xlsx'].indexOf(
        file.originalname.split('.')[file.originalname.split('.').length - 1]
      ) === -1
    ) {
      return callback(new Error('Wrong extension type'));
    }
    callback(null, true);
  }
}).single('file');

router.post('/data', function(req, res) {
  console.log('xlsx to json conversion');
  req.setTimeout(0);
  let exceltojson;
  upload(req, res, function(err) {
    if (err) {
      res.status(400).json({ error_code: 1, err_desc: err });
      return;
    }

    if (!req.file) {
      console.log('sdggh');
      res.status(400).json({ error_code: 1, err_desc: 'No file passed' });
      return;
    }

    if (
      req.file.originalname.split('.')[
        req.file.originalname.split('.').length - 1
      ] === 'xlsx'
    ) {
      exceltojson = xlsxtojson;
    } else {
      exceltojson = xlstojson;
    }

    try {
      exceltojson(
        {
          input: req.file.path, //the same path where we uploaded our file
          output: null, //since we don't need output.json
          lowerCaseHeaders: false
        },
        function(err, result) {
          if (err) {
            return res
              .status(400)
              .json({ error_code: 1, err_desc: err, data: null });
          }
          console.log(result);
          const result1 = result.map(o => ({
            propertyId: o['id'],
            name: o['name'],
            streetAddress: o['street_address'],
            locality: o['locality'],
            region: o['region'],
            postalCode: o['postal_code'],
            country: o['country'],
            latitude: o['latitude'],
            longitude: o['longitude'],
            isActive: true,
            created_at: new Date().toISOString(),
            updated_at: new Date().toISOString()
          }));
          const db = knex(config);
          db.transaction(function(tr) {
            return knex(config)
              .batchInsert('hotelproperties', result1)
              .transacting(tr);
          })
            .then(() => {
              db.destroy();
              return res.status(200).json({ message: 'success' });
            })
            .catch(error => {
              db.destroy();
              return res.status(400).json({ message: error.message });
            });
        }
      );
    } catch (e) {
      db.destroy();
      res.status(400).json({ error_code: 1, err_desc: 'Corrupted excel file' });
    }
  });
});
//to get all properties (csv file or xml response)
router.get('/hotelallproperties', async (req, res) => {
  try {
    const response = await axios({
      url:
        'https://book.integration2.testaroom.com/api/properties.xml?api_key=c017604d-72c1-5414-adde-a18b77add9e7&auth_token=1c042bb6-913b-5a64-ad85-2e720dbca5bc',
      method: 'GET',
      auth: {
        username: 'c017604d-72c1-5414-adde-a18b77add9e7',
        password: '1c042bb6-913b-5a64-ad85-2e720dbca5bc'
      }
    });
    const result = await parser.toJson(response.data, {
      object: true,
      reversible: false,
      coerce: false,
      sanitize: true,
      trim: true,
      arrayNotation: true,
      alternateTextNode: false
    });
    return res.status(200).json(result);
  } catch (error) {
    return res
      .status(error.response.status)
      .json({ message: error.response.statusText });
  }
});

// multiple properties general search

router.get('/hotelsearch', async (req, res) => {
  const db = knex(config);
  console.log('murali');
  const roomDet = JSON.parse(searchDetails(req.query)).map((x, i) => ({
    seqNum: i + 1,
    adultNum: x.filter(x => x === 18).length,
    childNum: x.filter(x => x < 18).length
  }));

  var date_diff_indays = function() {
    dt1 = new Date(req.query.checkIn);
    dt2 = new Date(req.query.checkOut);
    return Math.floor(
      (Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) -
        Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) /
        (1000 * 60 * 60 * 24)
    );
  };
  const numberofRooms = JSON.parse(searchDetails(req.query)).length;
  const numberofNights = date_diff_indays();
  console.log(numberofNights);
  console.log(numberofRooms);
  console.log(parseFloat(req.query.rc));
  const properties = await db
    .select('propertyId', 'name', 'streetAddress')
    .from('hotelproperties')
    .where('locality', req.query.destinationName);
  const allpropertyids = properties.map(x => x.propertyId);
  var params = new URLSearchParams();
  for (let i = 0; i < allpropertyids.length; ++i) {
    params.append('property_id[]', allpropertyids[i]);
  }

  console.log(
    `https://availability.integration2.testaroom.com/api/1.1/room_availability?rinfo=${searchDetails(
      req.query
    )}&check_in=${req.query.checkIn}&check_out=${
      req.query.checkOut
    }&api_key=ad890408-b5a3-5b27-be8a-ed425dae250d&auth_token=b88f94a5-2b42-5de5-96b1-8730c2a34cc3`
  );
  const response = await axios({
    url: `https://availability.integration2.testaroom.com/api/1.1/room_availability?rinfo=${searchDetails(
      req.query
    )}&check_in=${req.query.checkIn}&check_out=${
      req.query.checkOut
    }&api_key=ad890408-b5a3-5b27-be8a-ed425dae250d&auth_token=b88f94a5-2b42-5de5-96b1-8730c2a34cc3`,

    method: 'POST',
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    data: params
  });
  const result = await parser.toJson(response.data, {
    object: true,
    reversible: false,
    coerce: false,
    sanitize: true,
    trim: true,
    arrayNotation: true,
    alternateTextNode: false
  });

  // supplements = [
  // {
  //   suppId: 1,
  //   suppName: 'dfgg',
  //   typeName: '',
  //   supptType: 1,
  //   suppIsMandatory: false,
  //   suppChargeType: 'dfg',
  //   price: 12,
  //   publishPrice: 123,
  //   supplementAgeGroup: [
  //     {
  //       suppFrom: 1,
  //       suppTo: 2,
  //       suppQuantity: 2,
  //       suppPrice: 23
  //     }
  //   ]
  // }
  // ];

  const data = result['room-stays'][0]['room-stay'].map(x => ({
    hotelId: x.room[0]['hotel-id'][0],
    provider: 'GetARoom',
    hotelName: properties.filter(
      y => y.propertyId === x.room[0]['hotel-id'][0]
    )[0].name,
    description: '',
    starRating: 3,
    address: properties.filter(
      y => y.propertyId === x.room[0]['hotel-id'][0]
    )[0].streetAddress,
    location: '',
    hotelImageUrl: '',
    price: parseFloat(x['display-pricing'][0].total[0]),
    room: [
      {
        roomId: x.room[0]['room-id'][0],
        priceInfo: {
          occupancy: [
            {
              priceBreak: {
                priceR: x['display-pricing'][0]['nightly-rates'][0][
                  'nightly-rate'
                ].map((y, i) => ({
                  value: i,
                  price:
                    parseFloat(y.rate[0]['$t']) +
                    parseFloat(
                      parseFloat(x['display-pricing'][0].total[0]) -
                        parseFloat(x['display-pricing'][0].subtotal[0])
                    ) /
                      (numberofNights * numberofRooms),
                  //  price: parseFloat(y.rate[0]['$t']),
                  pricePublish: 0,
                  from: dateformat(y.date[0]),
                  to: ''
                }))
              },
              roomDet: roomDet
            }
          ]
        },
        discount: {
          from: '',
          to: '',
          disType: '',
          value: '',
          name: '',
          actualPrice: 0,
          price: 0
        },
        essentialInfo: [
          {
            text: '',
            fromDate: '',
            toDate: ''
          }
        ],
        baseAmount: 0,
        roomConfirmation: '',
        chargeConditions: [
          {
            charge: '',
            chargeAmount: 0,
            currency: 'USD',
            fromDay: 0,
            toDay: 0,
            firstDay: x['cancellation-deadline']
              ? x['cancellation-deadline'][0].$t
              : "doesn't having cancellation deadline",
            description: x['cancellation-policy']
              ? x['cancellation-policy'][0].$t
              : "doesn't having cancellation policy"
          }
        ],
        roomType: x.room[0]['title'][0]['$t'],
        roomTypeName: x.room[0]['title'][0]['$t'],
        price:
          parseFloat(x['display-pricing'][0].total[0]) /
          (numberofRooms * numberofNights),
        totalPrice:
          parseFloat(x['display-pricing'][0].total[0]) /
          parseFloat(numberofRooms),
        currency: result['room-stays'][0].request[0].currency[0],
        maxGuests: 1,
        provider: 'GetARoom',
        mealPlan: 'Room Only',
        supplements: x['fees-collected-at-property'][0]['xsi:nil']
          ? []
          : x['fees-collected-at-property'][0].fee.map((s, i) => ({
              suppId: i,
              suppName: s.name[0].$t,
              typeName: s.name[0].$t,
              supptType: i,
              suppIsMandatory: s.description[0]['xsi:nil'] ? true : false,
              suppChargeType: s.description[0]['xsi:nil']
                ? ''
                : s.description[0].$t,
              price: parseFloat(s.amount[0]),
              publishPrice: parseFloat(s.total[0]),
              supplementAgeGroup: [
                {
                  suppFrom: 1,
                  suppTo: 2,
                  suppQuantity: x['fees-collected-at-property'][0].fee.length,
                  suppPrice: parseFloat(s.total[0])
                }
              ]
            })),
        boardBases: [],
        rateKey: x['rate-plan-code'][0],
        marketId: '',
        contractId: '',
        // tax: `x['display-pricing'][0]['taxes'][0]['$t'],
        // tax: x['display-pricing'][0]['fees'][0]['$t'],
        tax: `tax:${x['display-pricing'][0]['taxes'][0]['$t']},fees:${
          x['display-pricing'][0]['fees'][0]['$t']
        },subtotal:${x['display-pricing'][0].subtotal[0]},total:${
          x['display-pricing'][0].total[0]
        }`,
        // tax: `subtotal:${x['display-pricing'][0].subtotal[0]}`,
        id: '0'
      }
    ],
    additionalCharges: '',
    city: '',
    propertyType: '',
    token: ''
  }));

  const keys = data.map(x => x.hotelId);
  let unique = [...new Set(keys)];

  let respon = [];
  for (let i = 0; i < unique.length; ++i) {
    respon.push(
      data
        .filter(y => y.hotelId === unique[i])
        .map(k => ({
          hotelId: k.hotelId,
          provider: k.provider,
          hotelName: k.hotelName,
          description: k.description,
          starRating: k.starRating,
          address: k.address,
          location: k.location,
          hotelImageUrl: k.hotelImageUrl,
          price: k.price,
          room: k.room[0],
          additionalCharges: k.additionalCharges,
          city: k.city,
          propertyType: k.propertyType,
          token: k.token
        }))
    );
  }

  return res.status(200).json(
    respon.map(j => ({
      hotelId: j[0].hotelId,
      provider: j[0].provider,
      hotelName: j[0].hotelName,
      description: j[0].description,
      starRating: j[0].starRating,
      address: j[0].address,
      location: j[0].location,
      hotelImageUrl: j[0].hotelImageUrl,
      price: parseFloat(
        Math.min.apply(null, j.map(x => x.room).map(y => parseFloat(y.price)))
      ),
      room: j.map(x => x.room),
      additionalCharges: j[0].additionalCharges,
      city: j[0].city,
      propertyType: j[0].propertyType,
      token: j[0].token
    }))
  );
  //return res.status(200).json(result);
  // } catch (error) {
  //   return res
  //     .status(error.response.status)
  //     .json({ message: error.response.statusText });
  // }
});
// to get hotel details(room details) without amenities
router.get('/hoteldetails', async (req, res) => {
  try {
    //  console.log(req.query);

    const db = knex(config);
    var imagesArray;
    const amenitiesImages = axios({
      url: `https://supply.integration2.testaroom.com/hotel/api/properties/${req.query.hotelId}.json`,
      method: 'GET',
      auth: {
        username: 'ad890408-b5a3-5b27-be8a-ed425dae250d',
        password: 'b88f94a5-2b42-5de5-96b1-8730c2a34cc3'
      }
    })
      .then(function(response) {
        imagesArray = response.data.images.map(x => x.urls.large);
      })
      .catch(function(error) {
        imagesArray = [];
      });

    const response = await axios({
      url: `https://availability.integration2.testaroom.com/api/1.1/properties/${
        req.query.hotelId
      }/room_availability?rinfo=${searchDetails(req.query)}&check_in=${
        req.query.checkIn
      }&check_out=${
        req.query.checkOut
      }&cancellation_rules=1&transaction_id=1234&api_key=ad890408-b5a3-5b27-be8a-ed425dae250d&auth_token=b88f94a5-2b42-5de5-96b1-8730c2a34cc3`,
      method: 'GET',
      auth: {
        username: 'ad890408-b5a3-5b27-be8a-ed425dae250d',
        password: 'b88f94a5-2b42-5de5-96b1-8730c2a34cc3'
      }
    });

    // console.log(amenitiesImages.data.map(x => x.images));
    const result = await parser.toJson(response.data, {
      object: true,
      reversible: false,
      coerce: false,
      sanitize: true,
      trim: true,
      arrayNotation: true,
      alternateTextNode: false
    });
    const hotelRoomTypeIds = result['room-stays'][0]['room-stay'].map(
      x => x.room[0]['room-id'][0]
    );
    const roomDetails = result['room-stays'][0]['room-stay'].map(x => ({
      roomType: x.room[0].title[0].$t,
      roomTypeCode: x.room[0]['room-id'][0],
      maxGuests: 2,
      pricePerNight: parseFloat(x['display-pricing'][0].total[0]),
      facilities: [],
      imageUrl: '',
      hotelRoomTypeIds: hotelRoomTypeIds
    }));

    const hoteldetails = result['room-stays'][0]['room-stay'][0].room[0];
    const properties = await db
      .select('propertyId', 'name', 'streetAddress', 'latitude', 'longitude')
      .from('hotelproperties')
      .where('propertyId', hoteldetails['hotel-id'][0]);

    const data = {
      hotelId: hoteldetails['hotel-id'][0],
      hotelName: properties[0].name,
      hotelFax: '',
      hotelPhone: '',
      address: properties[0].streetAddress,
      description: hoteldetails['description'][0]['$t'],
      hotelImages: imagesArray,
      amenities: [],
      roomDetails: roomDetails,
      latitude: properties[0].latitude,
      longitude: properties[0].longitude
    };
    // console.log(data);
    return res.status(200).json(data);
    // return res.status(200).json(result);
  } catch (error) {
    return res
      .status(error.response.status)
      .json({ message: error.response.statusText });
  }
});

router.get('/cancellationpolicies', async (req, res) => {
  try {
    console.log(dateformat('2019-06-20T14:00:00Z'));
    const db = knex(config);

    const response = await axios({
      url: `https://availability.integration2.testaroom.com/api/1.1/properties/${
        req.query.hotelId
      }/room_availability?rinfo=${searchDetails(req.query)}&check_in=${
        req.query.checkIn
      }&check_out=${
        req.query.checkOut
      }&cancellation_rules=1&transaction_id=1234&api_key=ad890408-b5a3-5b27-be8a-ed425dae250d&auth_token=b88f94a5-2b42-5de5-96b1-8730c2a34cc3`,
      method: 'GET',
      auth: {
        username: 'ad890408-b5a3-5b27-be8a-ed425dae250d',
        password: 'b88f94a5-2b42-5de5-96b1-8730c2a34cc3'
      }
    });
    const result = await parser.toJson(response.data, {
      object: true,
      reversible: false,
      coerce: false,
      sanitize: true,
      trim: true,
      arrayNotation: true,
      alternateTextNode: false
    });

    const data = result['room-stays'][0]['room-stay']
      .filter(
        x =>
          x.room[0]['room-id'][0] === req.query.roomId &&
          x['rate-plan-code'][0] === req.query.rateplanCode
      )
      .map(y => ({
        currency: 'USD',
        firstDay: dateformat(y['cancellation-deadline'][0]),
        description: y['cancellation-policy'][0].$t,
        deadline: dateformat(
          y['cancellation-penalties'][0]['cancellation-penalty'][0].deadline[0]
        )
      }));
    return res.status(200).json(data[0]);
    //  return res.status(200).json(result);
  } catch (error) {
    return res
      .status(error.response.status)
      .json({ message: error.response.statusText });
  }
});
// to get hotel amenities and their codes
router.post('/prebook', async (req, res) => {
  try {
    console.log(JSON.stringify(req.body));
    console.log(req.body.roomsInfo);
    const roominfo = JSON.stringify(
      req.body.roomsInfo.map(room =>
        Array.from({ length: room.adults }, () => 18).concat(room.childAges)
      )
    );

    const response = await axios({
      url: `https://availability.integration2.testaroom.com/api/1.1/properties/${
        req.body.hotelId
      }/room_availability?rinfo=${roominfo}&check_in=${dateformat(
        req.body.checkIn
      )}&check_out=${dateformat(
        req.body.checkOut
      )}&cancellation_rules=1&room_id=${
        req.body.roomsInfo[0].roomId
      }&rate_plan_code=${
        req.body.roomsInfo[0].rateKey
      }&api_key=ad890408-b5a3-5b27-be8a-ed425dae250d&auth_token=b88f94a5-2b42-5de5-96b1-8730c2a34cc3`,
      method: 'GET',
      auth: {
        username: 'ad890408-b5a3-5b27-be8a-ed425dae250d',
        password: 'b88f94a5-2b42-5de5-96b1-8730c2a34cc3'
      }
    });
    const result = await parser.toJson(response.data, {
      object: true,
      reversible: false,
      coerce: false,
      sanitize: true,
      trim: true,
      arrayNotation: true,
      alternateTextNode: false
    });
    console.log(result['room-stays'][0]);
    return res.status(200).json({
      status: result['room-stays'][0]['room-stay'] ? 'success' : 'failure'
    });
    // return res.status(200).json(result);
  } catch (error) {
    return res
      .status(error.response.status)
      .json({ message: error.response.statusText });
  }
});
//to book a room
router.post('/book', async (req, res) => {
  let ages1 = [];
  let ages2 = [];
  let ages3 = [];
  let ages4 = [];
  let ages5 = [];
  let ages6 = [];
  let ages7 = [];
  let ages8 = [];
  let ages9 = [];
  const numberofRooms = req.body.roomsInfo.length;
  for (i = 0; i < numberofRooms; ++i) {
    if (i === 0) {
      ages1 = ages1.concat(req.body.roomsInfo[i].childAges);
      for (j = 0; j < req.body.roomsInfo[i].adults; ++j) {
        ages1.push(18);
      }
    }
    if (i === 1) {
      ages2 = ages2.concat(req.body.roomsInfo[i].childAges);
      for (j = 0; j < req.body.roomsInfo[i].adults; ++j) {
        ages2.push(18);
      }
    }
    if (i === 2) {
      ages3 = ages3.concat(req.body.roomsInfo[i].childAges);
      for (j = 0; j < req.body.roomsInfo[i].adults; ++j) {
        ages3.push(18);
      }
    }
    if (i === 3) {
      ages4 = ages4.concat(req.body.roomsInfo[i].childAges);
      for (j = 0; j < req.body.roomsInfo[i].adults; ++j) {
        ages4.push(18);
      }
    }
    if (i === 4) {
      ages5 = ages5.concat(req.body.roomsInfo[i].childAges);
      for (j = 0; j < req.body.roomsInfo[i].adults; ++j) {
        ages5.push(18);
      }
    }
    if (i === 5) {
      ages6 = ages6.concat(req.body.roomsInfo[i].childAges);
      for (j = 0; j < req.body.roomsInfo[i].adults; ++j) {
        ages6.push(18);
      }
    }
    if (i === 6) {
      ages7 = ages7.concat(req.body.roomsInfo[i].childAges);
      for (j = 0; j < req.body.roomsInfo[i].adults; ++j) {
        ages7.push(18);
      }
    }
    if (i === 7) {
      ages8 = ages8.concat(req.body.roomsInfo[i].childAges);
      for (j = 0; j < req.body.roomsInfo[i].adults; ++j) {
        ages8.push(18);
      }
    }
    if (i === 8) {
      ages9 = ages9.concat(req.body.roomsInfo[i].childAges);
      for (j = 0; j < req.body.roomsInfo[i].adults; ++j) {
        ages9.push(18);
      }
    }
  }
  // let roomdetails = [];
  let roomages = [
    ages1,
    ages2,
    ages3,
    ages4,
    ages5,
    ages6,
    ages7,
    ages8,
    ages9
  ].filter(x => x.length !== 0);
  console.log(dateformat(req.body.checkIn));
  const rooms = req.body.roomsInfo.map((x, i) => ({
    given_name: x.guestDetails[0].firstName,
    family_name: x.guestDetails[0].lastName,
    guest_ages: roomages[i]
  }));
  console.log(
    JSON.stringify({
      version: '1.1',
      itinerary: {
        affiliate_session_code: 'Z1234314',
        affiliate_customer_code: '1343',
        affiliate_agent_code: '12343',
        point_of_sale: 'call_center',
        customer_ip_address: '4.2.2.1',
        customer_user_agent:
          'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_1) AppleWebKit/534.48.3 (KHTML, like Gecko) Version/5.1 Safari/534.48.3',
        customer_accept_language: 'en-US',
        customer_device_fingerprint: '4effbcd3705364e049fa7bddaad21e36',
        customer_timezone_offset: '300',
        reservations: [
          {
            rooms: rooms,
            affiliate_token: 'd6587f3a',
            tax: parseFloat(
              req.body.billingInfo.tax.split(',')[0].split(':')[1]
            ),
            fees: parseFloat(
              req.body.billingInfo.tax.split(',')[1].split(':')[1]
            ),
            subtotal: parseFloat(
              req.body.billingInfo.tax.split(',')[2].split(':')[1]
            ),
            total: parseFloat(
              req.body.billingInfo.tax.split(',')[3].split(':')[1]
            ),
            check_in: dateformat(req.body.checkIn),
            check_out: dateformat(req.body.checkOut),
            property_uuid: req.body.hotelId,
            room_type_uuid: req.body.roomsInfo[0].roomId,
            rate_plan_code: req.body.roomsInfo[0].rateKey,
            email: 'g@gmail.com'
          }
        ],
        payment: {
          guarantee_code: 'DEPOSIT'
        },
        custom_fields: {
          confirmation: Math.random()
            .toString(36)
            .substring(4)
        }
      }
    })
  );
  const response = await axios({
    url: `https://book.integration2.testaroom.com/api/itineraries.json?api_key=ad890408-b5a3-5b27-be8a-ed425dae250d&auth_token=b88f94a5-2b42-5de5-96b1-8730c2a34cc3`,
    method: 'POST',
    auth: {
      username: 'ad890408-b5a3-5b27-be8a-ed425dae250d',
      password: 'b88f94a5-2b42-5de5-96b1-8730c2a34cc3'
    },
    data: {
      version: '1.1',
      itinerary: {
        affiliate_session_code: 'Z1234314',
        affiliate_customer_code: '1343',
        affiliate_agent_code: '12343',
        point_of_sale: 'call_center',
        customer_ip_address: '4.2.2.1',
        customer_user_agent:
          'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_1) AppleWebKit/534.48.3 (KHTML, like Gecko) Version/5.1 Safari/534.48.3',
        customer_accept_language: 'en-US',
        customer_device_fingerprint: '4effbcd3705364e049fa7bddaad21e36',
        customer_timezone_offset: '300',
        reservations: [
          {
            rooms: rooms,
            affiliate_token: '24c5e46d',
            tax: parseFloat(
              req.body.billingInfo.tax.split(',')[0].split(':')[1]
            ),
            fees: parseFloat(
              req.body.billingInfo.tax.split(',')[1].split(':')[1]
            ),
            subtotal: parseFloat(
              req.body.billingInfo.tax.split(',')[2].split(':')[1]
            ),
            total: parseFloat(
              req.body.billingInfo.tax.split(',')[3].split(':')[1]
            ),
            check_in: dateformat(req.body.checkIn),
            check_out: dateformat(req.body.checkOut),
            property_uuid: req.body.hotelId,
            room_type_uuid: req.body.roomsInfo[0].roomId,
            rate_plan_code: req.body.roomsInfo[0].rateKey,
            email: 'g@gmail.com'
          }
        ],
        payment: {
          guarantee_code: 'DEPOSIT'
        },
        custom_fields: {
          confirmation: Math.random()
            .toString(36)
            .substring(4)
        }
      }
    }
  });
  const data = {
    reservations: [
      {
        reservationNumber:
          response.data.reservations[0].getaroom_reservation_token,
        price: parseFloat(response.data.reservations[0].total)
      }
    ],
    reservationGroupId: response.data.itinerary_audit_log_id,
    cancellationPolicies: []
  };

  return res.status(200).json(data);
  // } catch (error) {
  //   return res
  //     .status(error.response.status)
  //     .json({ message: error.response.statusText });
  // }
});

//hotel booking confirmation
router.get('/bookingconfirmation', async (req, res) => {
  try {
    const response = await axios({
      url:
        'https://book.integration2.testaroom.com/api/itineraries/f05c9a96-9b5e-4092-a1c3-0908cb6023ac.json?api_key=ad890408-b5a3-5b27-be8a-ed425dae250d&auth_token=b88f94a5-2b42-5de5-96b1-8730c2a34cc3',
      method: 'GET',
      auth: {
        username: 'ad890408-b5a3-5b27-be8a-ed425dae250d',
        password: 'b88f94a5-2b42-5de5-96b1-8730c2a34cc3'
      }
    });
    return res.status(200).json(response.data);
  } catch (error) {
    return res
      .status(error.response.status)
      .json({ message: error.response.statusText });
  }
});

// room cancellation
router.get('/cancellation', async (req, res) => {
  try {
    const response = await axios({
      url: `https://book.integration2.testaroom.com/api/itineraries/${req.query.reservationid}.json?api_key=c017604d-72c1-5414-adde-a18b77add9e7&auth_token=1c042bb6-913b-5a64-ad85-2e720dbca5bc`,
      method: 'PUT',
      auth: {
        username: 'c017604d-72c1-5414-adde-a18b77add9e7',
        password: '1c042bb6-913b-5a64-ad85-2e720dbca5bc'
      },
      data: { itinerary: { state: 'cancelled' } }
    });
    return res.status(200).json(response.data);
  } catch (error) {
    return res
      .status(error.response.status)
      .json({ message: error.response.statusText });
  }
});

router.get('/hotelsearch1', async (req, res) => {
  try {
    const db = knex(config);

    const roomDet = JSON.parse(searchDetails(req.query)).map((x, i) => ({
      seqNum: i + 1,
      adultNum: x.filter(x => x === 18).length,
      childNum: x.filter(x => x < 18).length
    }));

    var date_diff_indays = function() {
      dt1 = new Date(req.query.checkIn);
      dt2 = new Date(req.query.checkOut);
      return Math.floor(
        (Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) -
          Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) /
          (1000 * 60 * 60 * 24)
      );
    };
    const numberofRooms = JSON.parse(searchDetails(req.query)).length;
    const numberofNights = date_diff_indays();
    console.log(roomDet);
    const properties = await db
      .select('propertyId', 'name', 'streetAddress')
      .from('hotelproperties')
      .where('locality', req.query.destinationName);
    const allpropertyids = properties.map(x => x.propertyId);
    var params = new URLSearchParams();
    for (let i = 0; i < allpropertyids.length; ++i) {
      params.append('property_id[]', allpropertyids[i]);
    }

    const response = await axios({
      url: `https://availability.integration2.testaroom.com/api/1.1/room_availability?rinfo=${searchDetails(
        req.query
      )}&check_in=${req.query.checkIn}&check_out=${
        req.query.checkOut
      }&api_key=c017604d-72c1-5414-adde-a18b77add9e7&auth_token=1c042bb6-913b-5a64-ad85-2e720dbca5bc`,

      method: 'POST',
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      data: params
    });
    const result = await parser.toJson(response.data, {
      object: true,
      reversible: false,
      coerce: false,
      sanitize: true,
      trim: true,
      arrayNotation: true,
      alternateTextNode: false
    });

    const data = result['room-stays'][0]['room-stay'].map(x => ({
      hotelId: x.room[0]['hotel-id'][0],
      provider: 'GetARoom',
      hotelName: properties.filter(
        y => y.propertyId === x.room[0]['hotel-id'][0]
      )[0].name,
      description: '',
      starRating: 3,
      address: properties.filter(
        y => y.propertyId === x.room[0]['hotel-id'][0]
      )[0].streetAddress,
      location: '',
      hotelImageUrl: '',
      price: parseFloat(x['display-pricing'][0].subtotal[0]),
      room: [
        {
          roomId: x.room[0]['room-id'][0],
          priceInfo: {
            occupancy: [
              {
                priceBreak: {
                  priceR: x['display-pricing'][0]['nightly-rates'][0][
                    'nightly-rate'
                  ].map((y, i) => ({
                    value: i,
                    price: parseFloat(y.rate[0]),
                    pricePublish: 0,
                    from: dateformat(y.date[0]),
                    to: ''
                  }))
                },
                roomDet: roomDet
              }
            ]
          },
          discount: {
            from: '',
            to: '',
            disType: '',
            value: '',
            name: '',
            actualPrice: 0,
            price: 0
          },
          essentialInfo: [
            {
              text: '',
              fromDate: '',
              toDate: ''
            }
          ],
          baseAmount: 0,
          roomConfirmation: '',
          chargeConditions: [
            {
              charge: '',
              chargeAmount: 0,
              currency: 'USD',
              fromDay: 0,
              toDay: 0,
              firstDay: x['cancellation-deadline']
                ? x['cancellation-deadline'][0].$t
                : "doesn't having cancellation deadline",
              description: x['cancellation-policy']
                ? x['cancellation-policy'][0].$t
                : "doesn't having cancellation policy"
            }
          ],
          roomType: x.room[0]['title'][0]['$t'],
          roomTypeName: x.room[0]['title'][0]['$t'],
          price:
            parseFloat(x['display-pricing'][0].subtotal[0]) /
            (numberofRooms * numberofNights),
          totalPrice:
            parseFloat(x['display-pricing'][0].subtotal[0]) / req.query.rc,
          currency: result['room-stays'][0].request[0].currency[0],
          maxGuests: 1,
          provider: 'GetARoom',
          mealPlan: 'Room Only',
          supplements: [],
          boardBases: [],
          rateKey: x['rate-plan-code'][0],
          marketId: '',
          contractId: '',
          tax: 0,
          id: '0'
        }
      ],
      additionalCharges: '',
      city: '',
      propertyType: '',
      token: ''
    }));

    const keys = data.map(x => x.hotelId);
    let unique = [...new Set(keys)];

    let respon = [];
    for (let i = 0; i < unique.length; ++i) {
      respon.push(
        data
          .filter(y => y.hotelId === unique[i])
          .map(k => ({
            hotelId: k.hotelId,
            provider: k.provider,
            hotelName: k.hotelName,
            description: k.description,
            starRating: k.starRating,
            address: k.address,
            location: k.location,
            hotelImageUrl: k.hotelImageUrl,
            price: k.price,
            room: k.room[0],
            additionalCharges: k.additionalCharges,
            city: k.city,
            propertyType: k.propertyType,
            token: k.token
          }))
      );
    }

    return res.status(200).json(
      respon.map(j => ({
        hotelId: j[0].hotelId,
        provider: j[0].provider,
        hotelName: j[0].hotelName,
        description: j[0].description,
        starRating: j[0].starRating,
        address: j[0].address,
        location: j[0].location,
        hotelImageUrl: j[0].hotelImageUrl,
        price: Math.min.apply(null, j.map(x => x.room).map(y => y.price)),
        room: j.map(x => x.room),
        additionalCharges: j[0].additionalCharges,
        city: j[0].city,
        propertyType: j[0].propertyType,
        token: j[0].token
      }))
    );
    // return res.status(200).json(result);
  } catch (error) {
    return res
      .status(error.response.status)
      .json({ message: error.response.statusText });
  }
});

// router.get('/devicerecordsdownload', async (req, res) => {
//   try {
//     let db = knex(config);
//    const hotelids=

//     // jsonexport(logdata, function(err, csv) {
//     //   if (err) return console.log(err);
//     //   fs.writeFileSync('./uploads/devicedata.csv', csv, 'binary');
//     // });

//     // return res.download('./uploads/devicedata.csv', 'devicedata.csv');
//     return res.json('success');
//   } catch (error) {
//     await db.destroy();
//     return res.status(500).json({
//       message: 'some thing awefull happened'
//     });
//   }
// });

module.exports = router;

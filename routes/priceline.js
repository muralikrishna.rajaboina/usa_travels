const express = require('express');
const router = express.Router();
const axios = require('axios');
const converter = require('xml2json');
const knex = require('knex');
const config = require('../knexfile').development;

const searchDetails = request => {
  let r1Details = [];
  let r2Details = [];
  let r3Details = [];
  let r4Details = [];
  let r5Details = [];
  let r6Details = [];
  let r7Details = [];
  let r8Details = [];
  let r9Details = [];
  if (request.r1a) {
    for (let j = 0; j < request.r1a; ++j) {
      r1Details.push(18);
    }
    if (request.r1ka) {
      r1Details = r1Details
        .concat(request.r1ka.slice(0, request.r1k))
        .map(x => parseFloat(x));
    }
  }

  if (request.r2a) {
    for (let j = 0; j < request.r2a; ++j) {
      r2Details.push(18);
    }
    if (request.r2ka) {
      r2Details = r2Details
        .concat(request.r2ka.slice(0, request.r2k))
        .map(x => parseFloat(x));
    }
  }
  if (request.r3a) {
    for (let j = 0; j < request.r3a; ++j) {
      r3Details.push(18);
    }
    if (request.r3ka) {
      r3Details = r3Details
        .concat(request.r3ka.slice(0, request.r3k))
        .map(x => parseFloat(x));
    }
  }
  if (request.r4a) {
    for (let j = 0; j < request.r4a; ++j) {
      r4Details.push(18);
    }
    if (request.r4ka) {
      r4Details = r4Details
        .concat(request.r4ka.slice(0, request.r4k))
        .map(x => parseFloat(x));
    }
  }
  if (request.r5a) {
    for (let j = 0; j < request.r5a; ++j) {
      r5Details.push(18);
    }
    if (request.r5ka) {
      r5Details = r5Details
        .concat(request.slice(0, request.r5k))
        .map(x => parseFloat(x));
    }
  }

  if (request.r6a) {
    for (let j = 0; j < request.r6a; ++j) {
      r6Details.push(18);
    }
    if (request.r6ka) {
      r6Details = r6Details
        .concat(request.r6ka.slice(0, request.r6k))
        .map(x => parseFloat(x));
    }
  }
  if (request.r7a) {
    for (let j = 0; j < request.r7a; ++j) {
      r7Details.push(18);
    }
    if (request.r7ka) {
      r7Details = r7Details
        .concat(request.r7ka.slice(0, request.r7k))
        .map(x => parseFloat(x));
    }
  }
  if (request.r8a) {
    for (let j = 0; j < request.r8a; ++j) {
      r8Details.push(18);
    }
    if (request.r8ka) {
      r8Details = r8Details
        .concat(request.r8ka.slice(0, request.r8k))
        .map(x => parseFloat(x));
    }
  }
  if (request.r9a) {
    for (let j = 0; j < request.r9a; ++j) {
      r9Details.push(18);
    }
    if (request.r9ka) {
      r9Details = r9Details
        .concat(request.r9ka.slice(0, request.r9k))
        .map(x => parseFloat(x));
    }
  }
  let roomsDetails = JSON.stringify(
    [
      r1Details,
      r2Details,
      r3Details,
      r4Details,
      r5Details,
      r6Details,
      r7Details,
      r8Details,
      r9Details
    ].filter(x => x.length !== 0)
  );
  return roomsDetails;
};

// const multer = require('multer');
const dateformat = date => {
  let day = '';
  if (new Date(date).getDate() < 10) {
    day = '0' + new Date(date).getDate();
  } else {
    day = new Date(date).getDate();
  }

  let month = '';
  if (new Date(date).getMonth() + 1 < 10) {
    month = '0' + (new Date(date).getMonth() + 1);
  } else {
    month = new Date(date).getMonth() + 1;
  }
  const newdate = new Date(date).getFullYear() + '-' + month + '-' + day;

  return newdate;
};
router.get('/destinationsdata', async function(req, res, next) {
  try {
    const response = await axios({
      url:
        'https://api.rezserver.com/api/shared/getBOF2.Downloads.Hotel.Cities?api_key=931a7692835e3495e73d5f03a48871f1&refid=8488',
      method: 'GET',
      headers: { format: 'json', Accept: 'application/json' }
    });
    const response_json = await converter.toJson(response.data, {
      object: true,
      reversible: false,
      coerce: false,
      sanitize: true,
      trim: true,
      arrayNotation: true,
      alternateTextNode: false
    });
    var result =
      response_json['getSharedBOF2.Downloads.Hotel.Cities'][0]['results'][0][
        'cities'
      ][0]['city'];
    const result1 = result.map(o => ({
      destinationCode: o['cityid_ppn'][0],
      destinationName: o['city'][0],
      stateCode:
        typeof o['state_code'][0] === 'object' ? '' : o['state_code'][0],
      stateName: typeof o['state'][0] === 'object' ? '' : o['state'][0],
      countryCode: o['country_code'][0],
      countryName: o['country'][0],
      latitude: o['latitude'][0],
      longitude: o['longitude'][0],
      hotelCount: o['hotel_count'][0],
      isActive: true,
      created_at: new Date().toISOString(),
      updated_at: new Date().toISOString()
    }));
    const db = knex(config);
    db.transaction(function(tr) {
      return knex(config)
        .batchInsert('pricelinedestinations', result1)
        .transacting(tr);
    })
      .then(() => {
        db.destroy();
        return res.status(200).json({ message: 'success' });
      })
      .catch(error => {
        db.destroy();
        return res.status(400).json({ message: error.message });
      });
    //  return res.send(result)
  } catch (err) {
    return res.status(500).json(err);
  }
});

router.get('/propertiesdata', async function(req, res, next) {
  try {
    const response = await axios({
      url:
        'https://api.rezserver.com/api/shared/getBOF2.Downloads.Hotel.Hotels?api_key=931a7692835e3495e73d5f03a48871f1&refid=8488',
      method: 'GET',
      headers: { format: 'json', Accept: 'application/json' }
    });
    const response_json = await converter.toJson(response.data, {
      object: true,
      reversible: false,
      coerce: false,
      sanitize: true,
      trim: true,
      arrayNotation: true,
      alternateTextNode: false
    });
    var result =
      response_json['getSharedBOF2.Downloads.Hotel.Hotels'][0]['results'][0][
        'hotels'
      ][0]['hotel'];
    const result1 = result.map(o => ({
      propertyId: o['hotelid_ppn'][0],
      name: o['hotel_name'][0],
      rating: o['star_rating'][0],
      destinationCode: o['cityid_ppn'][0],
      destinationName: o['city'][0],
      stateCode:
        typeof o['state_code'][0] === 'object' ? '' : o['state_code'][0],
      stateName: typeof o['state'][0] === 'object' ? '' : o['state'][0],
      countryCode: o['country_code'][0],
      countryName: o['country'][0],
      address: o['hotel_address'][0],
      postalCode: o['postal_code'][0],
      latitude: o['latitude'][0],
      longitude: o['longitude'][0],
      isActive: true,
      created_at: new Date().toISOString(),
      updated_at: new Date().toISOString()
    }));
    const db = knex(config);
    db.transaction(function(tr) {
      return knex(config)
        .batchInsert('pricelineProperties', result1)
        .transacting(tr);
    })
      .then(() => {
        db.destroy();
        return res.status(200).json({ message: 'success' });
      })
      .catch(error => {
        db.destroy();
        return res.status(400).json({ message: error.message });
      });
    //    return res.send(result)
  } catch (err) {
    return res.status(500).json(err);
  }
});

router.get('/hotelsearch', async function(req, res, next) {
  //   try {
  const db = knex(config);

  const properties = await db
    .select('propertyId', 'name', 'rating', 'address', 'destinationName')
    .from('pricelineProperties');

  propertyIds = String(
    properties.map(x => x.propertyId).filter((x, i) => i < 500)
  );
  let checkindate = new Date('2019-09-09');
  const roomDet = JSON.parse(searchDetails(req.query)).map((x, i) => ({
    seqNum: i + 1,
    adultNum: x.filter(x => x === 18).length,
    childNum: x.filter(x => x < 18).length
  }));
  var date_diff_indays = function() {
    dt1 = new Date(req.query.checkIn);
    dt2 = new Date(req.query.checkOut);
    return Math.floor(
      (Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) -
        Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) /
        (1000 * 60 * 60 * 24)
    );
  };
  const numberofRooms = JSON.parse(searchDetails(req.query)).length;
  const numberofNights = date_diff_indays();
  const response = await axios({
    url:
      'https://api.rezserver.com/api/hotel/getRates.Live.Multi?api_key=931a7692835e3495e73d5f03a48871f1&refid=8488',
    method: 'GET',
    params: {
      check_in: '2019-09-09',
      check_out: '2019-09-12',
      hotel_ids: propertyIds,
      adults: 2,
      children: 2,
      rooms: 2,
      format: 'json'
    }
  });

  const hotelsdata =
    response.data['getHotelRates.Live.Multi']['results']['hotel_data'];
  const data = Object.entries(hotelsdata).map(x => ({
    hotelId: x[1].id,
    provider: 'Priceline',
    hotelName: properties.filter(y => parseInt(y.propertyId) === x[1].id)[0]
      .name,
    description: '',
    starRating: properties.filter(y => parseInt(y.propertyId) === x[1].id)[0]
      .rating,
    address: properties.filter(y => parseInt(y.propertyId) === x[1].id)[0]
      .address,
    location: properties.filter(y => parseInt(y.propertyId) === x[1].id)[0]
      .destinationName,
    hotelImageUrl: '',
    price: Math.min.apply(
      null,
      Object.entries(x[1]['rate_data'])
        .map(r =>
          Object.entries(r[1].price_details.night_price_data).map(
            y => y[1].display_night_price
          )
        )
        .map(k => Math.min.apply(null, k))
    ),
    room: Object.entries(x[1]['rate_data']).map(r => ({
      room: [
        {
          roomId: r[1].rate_tracking_id,
          priceInfo: {
            occupancy: [
              {
                priceBreak: {
                  priceR: Object.entries(
                    r[1].price_details.night_price_data
                  ).map((nightprice, i) => ({
                    value: i,
                    price: parseFloat(nightprice[1].source_night_price),
                    pricePublish: parseFloat(nightprice[1].display_night_price),
                    from: dateformat(
                      checkindate.setDate(checkindate.getDate() + i)
                    ),
                    to: ''
                  }))
                },
                roomDet: roomDet
              }
            ]
          },
          discount: {
            from: '',
            to: '',
            disType: '',
            value: '',
            name: '',
            actualPrice: 0,
            price: 0
          },
          essentialInfo: [
            {
              text: '',
              fromDate: '',
              toDate: ''
            }
          ],
          baseAmount: 0,
          roomConfirmation: '',
          chargeConditions: [
            {
              charge: '',
              chargeAmount: 0,
              currency: 'EUR',
              fromDay: 0,
              toDay: 0,
              firstDay: '2019-10-10',
              description: ''
            }
          ],
          roomType: r[1].rate_type,
          roomTypeName: r[1].rate_type,

          price:
            parseFloat(r[1].price_details.display_total) /
            (numberofNights * numberofRooms),
          totalPrice: parseFloat(r[1].price_details.display_total),
          currency: r[1].price_details.source_currency,
          maxGuests: 1,
          provider: 'Priceline',
          mealPlan: 'Room Only',
          supplements: [],
          boardBases: [],
          rateKey: r[1].rate_plan_code,
          marketId: '',
          contractId: '',
          tax: r[1].price_details.source_taxes,
          id: '0'
        }
      ]
    })),
    additionalCharges: '',
    city: '',
    propertyType: '',
    token: ''
  }));

  return res.send(data).catch(error => {
    db.destroy();
    return res.status(400).json({ message: error });
  });
  //   } catch (err) {
  //     return res.status(500).json(err);
  //   }
});

router.get('/hoteldetails', async function(req, res, next) {
  try {
    const response = await axios({
      url:
        'https://api.rezserver.com/api/hotel/getHotelDetails?api_key=931a7692835e3495e73d5f03a48871f1&refid=8488',
      method: 'GET',
      params: { hotel_id: '700031241', format: 'json' }
    });
    const result =
      response.data['getHotelHotelDetails']['results']['hotel_data']['hotel_0'];
    return res.send(result);
  } catch (err) {
    return res.status(500).json(err);
  }
});

router.get('/book', async function(req, res, next) {
  try {
    const response = await axios({
      url:
        'https://api.rezserver.com/api/hotel/getBookRequest?api_key=931a7692835e3495e73d5f03a48871f1&refid=8488',
      method: 'POST',
      params: { hotel_id: '700031241', format: 'json' },
      data: {
        name_first: 'Mario',
        name_last: 'Mario',
        email: 'yoshi.mario@nintendo.com',
        phone_number: 2505551212,
        'guest_name_first[1]': 'Fred',
        'guest_name_last[1]': 'Flintstone',
        'guest_name_first[2]': 'Wilma',
        'guest_name_last[2]': 'Flintstone',
        card_type: 'Visa',
        card_number: 4111111111111111,
        expires: '082019',
        cvc_code: 591,
        card_holder: 'Hotelsbycity%20Operations',
        address_line_one: '301%20Cobblestone%20Way',
        address_city: 'Bedrock',
        address_postal_code: '14139',
        address_state_code: 'NY',
        country_code: 'US',
        initials: 'CJ',
        allow_duplicate: 1,
        ppn_bundle:
          'HCRQ1--_eJwBoAVf_puvecULaUID3DsGBVdX7UWGyVHK3UVygpCUWAUoUIA4rDgjLxjmFSmeNG0ZBDp0Fm5NuY_pgz_fCPQ_fQRe_f_pm_fdtHWLl4wvHcfz5YH0QrJ_f_pKCgcNFHInxEfYm9IwGKgpJ3HrSNP8pZsVEo0N3DzBMQAgSGzD_pa2OYRnB9ZL18lA_fvyoOpDdVo6G2GIc3xDPSubJVOhJNya15AJcpFdah1pzSdxyer_pV_fl8F_fmz3bKn_pZUXzb_fr3XORxQs0ndZZr4dx0RT4mVdviPgAcmGefAXCEuztSlSlbU_fKSxTVEwKatUrZJknu1qr_fI9Z8FhRL5GaqRuWBklojQsj2q0rPIU4hjOTByTTUTX4_pDD0HZTo_f9HdaAWxtekcJWh33OvZicq9nAVlfr_pMoPC1u_pcIHvvOckfUvVklIM7EZfSO4BDf9IvmtbAeAltwHOX612cWBbirRh203Jz3JtI3ooFYhi4Q2UU9NI5muSiJ6bZWNSuoTXZNgWXhryFEZI2XteRSTaBBK5Zezdzj0UWJurOLv_fi_fVQddVc4S6PFo1YNnF_fb1YrJSopvb14fKuKOv3hB1BmiGVTFL1SoO7QncgmFt0qNrC3ny0A0TDgMBv2enE7O_pySjD9iicKtC3P25kkOz_fZ7SvQcF9kJy7q_fNjVhDLG1pAPjRVfIESs08stvIIUGjWv_fYatnliYGYMjF7yGMLyJCcEHVLEhfJ4kqIA4R33VbMy6P0WxYOJKFOdd2eT_p2mx74wnSPVmIMtQ4IVWIBj6l9GBjNhmHfhK1ANvmYtxK8SN_fbgcKzKTx_p2G9uMTWw5mfB01GVzp4gbdAl0TpD33VZy8xG563tRzsFvYusAPCCwZA3j1MTUoNx7fVqRSBowrx0PUaaX9_fNvBWLrAMnm00dn3TJ_pop6oxhkhIVe8IcW8w2WRtEif0n76JDWxomGUDzdNg7UvQY1q_pUSJ4jxBeVfYtAp9Cob4MqijEy2uLq5PacClgugiHFUMbjs_pHub_fIunRJNY4Bu9oaXuq6yamrAlS1eAxu62pt_pavH9pdu0C3aSxYcg8A6gfMgkqrkfQfbXcQiy0SU32XVm2WFzsHg_fqKaGuJMd6PbzvG2WRqzzTotaUwmmK9exkMZWW_fbn8u61PsOZIKBkDqOpvA1bffE_pfbISuRJc7Ln2d_fsTM43gTxn4u0a3V5N5OFtBGknK7COP4JEl0rYZWhGFf9_faQseITUPKJx5jxEM6dEwS3wdjQAuLkFarDN11oK5FuD7xO7q2B4g9HPwHo_fpukLr81wdVkIHIxOqEdb_fUBkl1A5Q_p9ODQUgrbkqxpEOAtk6UfN7QiMI_fslmdWCwmqJ_poeQEY96_fkrlE5B6qHUncCgMSEHkwKkZvmq0yuDeEWcf4W7I0oTwhbbKmsl78pBcEMOJfN39L0l6wtCmtDQXyQyirdck_pfzi_pMjpdWvVgroj2lWDWB3BfKHVzMNRxLiWSp7_pCXa9AxX9y91tkmHbLoUNX8M78dkeYhX1kHiJms1_f_flWjy7jVLFbo_f_f8MfzFWvyANoo2x8h2ZUe8xZq8JvLhyUtHn75_p_fc8S8_p6bZatXgHsol_fKGojD9X5DcC2V7wgnGNXDERacvtx5ffY1Oop_pgzShKNTPGShp8oWQK67V1KCyv2Dc60I22TSAyKToZyorWSzfHhJG_pb_f_fuKYyiQVhtbW3FMZry64UEQ_pcoXyXxhcc71Ue5VRQTEC_fNfJV3hYDR_fjrZ2QpBnt7QwYdS3CKa_psSuU_fGfo6Als_fQg_f5Gj7EHTFWC9MLxMqdj04D3FWmBM9WP2VkcNiY9y4K1ofybfQ5_psw4YubW1p0VNYE0j9wbDbSQfFJhdApQzWiby_pgN6D1f2QEKnBP7WLuAtErr2_fDrbMT1Hk1gzLSX5p_fiGo5Dkn9VwhoD17haPmUgsKSv26_pF024D77gWTzSI'
      }
    });

    return res.send(response.data);
  } catch (err) {
    return res.status(500).json(err);
  }
});

module.exports = router;
